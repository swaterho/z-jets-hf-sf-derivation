import json
import re
import os
import sys

"""
=================================
== Sample Categoriser ==
=================================

Description:
    - When running an L2 locally, you need to match your L1 root files by ntuple DSID. This does that for you.

Usage:
    - ./sample-categoriser.py /path/to/dsid_map.json /path/to/sample_summary.txt /path/to/root_files_list.txt /path/to/output_directory 

Notes:
    

"""

def categorise_paths(categories_json_path, summary_txt_path, paths_txt_path, output_directory):
    # Ensure the output directory exists
    os.makedirs(output_directory, exist_ok=True)

    # Load the categories from the JSON file
    with open(categories_json_path, 'r') as json_file:
        categories_data = json.load(json_file)

    # Map DSIDs to category names
    dsid_to_category = {}
    for category, details in categories_data.items():
        for dsid in details['dsids']:
            dsid_to_category[str(dsid)] = category

    # Read the summary file to map category names to full names
    category_to_fullname = {}
    with open(summary_txt_path, 'r') as summary_file:
        for line in summary_file:
            fullname = line.strip()
            shortname = fullname.split('.')[1]  # Extract the short name
            category_to_fullname[shortname] = fullname

    # Process the paths and categorise by campaign
    paths_by_category_and_campaign = {}
    campaign_mapping = {
        'r9364': 'mc16a',
        'r10201': 'mc16d',
        'r10724': 'mc16e'
    }
    path_count = 0
    unclassified_paths = []

    with open(paths_txt_path, 'r') as paths_file:
        for path in paths_file:
            path = path.strip()  # Strip any leading/trailing whitespace/newline characters
            classified = False  # Flag to check if path is classified

            # Check if the path is data
            if 'data' in path:
                # Extract the year from the path (assuming it's in the format 'data15_13TeV')
                year_match = re.search(r'data(\d{2})_13TeV', path)
                if year_match:
                    year = '20' + year_match.group(1)
                    category_shortname = f"data_{year}"
                    if category_shortname in category_to_fullname:
                        fullname = category_to_fullname[category_shortname]
                        if fullname not in paths_by_category_and_campaign:
                            paths_by_category_and_campaign[fullname] = []
                        paths_by_category_and_campaign[fullname].append(path)
                        path_count += 1
                        classified = True

            # Extract DSID from the path if it's not data
            if not classified:
                dsid_match = re.search(r'\.(\d{6})\.', path)
                # Extract campaign identifier from the path
                campaign_match = re.search(r'(r9364|r10201|r10724)', path)
                
                if dsid_match and campaign_match:
                    dsid = dsid_match.group(1)
                    campaign = campaign_match.group(1)
                    if dsid in dsid_to_category:
                        # Map the DSID to its category short name
                        category_shortname = dsid_to_category[dsid]
                        # Determine if 'AFII' is in the path
                        if 'a875' in path:
                            expected_fullname = f"2l.{category_shortname}_{campaign_mapping[campaign]}_AFII"
                        else:
                            expected_fullname = f"2l.{category_shortname}_{campaign_mapping[campaign]}"

                        if expected_fullname in category_to_fullname.values():
                            category_campaign_key = expected_fullname
                            if category_campaign_key not in paths_by_category_and_campaign:
                                paths_by_category_and_campaign[category_campaign_key] = []
                            paths_by_category_and_campaign[category_campaign_key].append(path)
                            # Increment the counter
                            path_count += 1
                            classified = True
                        else:
                            print(f"  Full category name not found for: {expected_fullname}")

            if not classified:
                unclassified_paths.append(path)
                print(f"  Unclassified path: {path}")

    # Print the total count after processing all paths
    print(f"Total paths processed and added to categories: {path_count}")

    # Write unclassified paths to an output file
    unclassified_output_path = os.path.join(output_directory, "unclassified_paths.txt")
    with open(unclassified_output_path, 'w') as unclassified_file:
        for path in unclassified_paths:
            unclassified_file.write(f"{path}\n")

    print("Unclassified paths written to unclassified_paths.txt")

    # Write the paths to files named after the full category names with campaigns
    for category_campaign, paths in paths_by_category_and_campaign.items():
        if 'data' in category_campaign:
            filename = os.path.join(output_directory, f"{category_campaign.replace('/', '_')}")
        else:
            # Remove redundant campaign identifiers from the filename
            for campaign in campaign_mapping.values():
                if f'_{campaign}_{campaign}' in category_campaign:
                    category_campaign = category_campaign.replace(f'_{campaign}_{campaign}', f'_{campaign}')
            filename = os.path.join(output_directory, f"{category_campaign.replace('/', '_')}.no_syst")
        with open(filename, 'w') as file:
            for path in paths:
                file.write(f"{path}\n")

    print("Done categorising paths.")

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]} <categories_json_path> <summary_txt_path> <paths_txt_path> <output_directory>")
        sys.exit(1)

    categories_json_path = sys.argv[1]
    summary_txt_path = sys.argv[2]
    paths_txt_path = sys.argv[3]
    output_directory = sys.argv[4]

    categorise_paths(categories_json_path, summary_txt_path, paths_txt_path, output_directory)